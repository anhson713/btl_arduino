#include <Arduino.h>
#include "DHT.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <string.h>
const char* ssid = "XXXXXXXX";
const char* password = "XXXXXXXXXX";
const char* serverName = "http://192.168.0.2:8080/api/v1/lights";
const int LIGHTPIN  = 5;
void setup() {
  Serial.begin(9600);
  pinMode(LIGHTPIN, INPUT);
    //setup wifi
  WiFi.begin(ssid,password);
  Serial.println("conecting");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}
 
void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  int value = digitalRead(LIGHTPIN);
  Serial.print("Gia tri : ");
  Serial.println(value);
  if(WiFi.status() == WL_CONNECTED) {
    WiFiClient client;
    HTTPClient http;
    http.begin(client, serverName);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST("{\"digitalRead\" :" + String(value) + "}");
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
        
      // Free resources
      http.end();
  } else {
    Serial.println("WiFi Disconnected");
  }
}
