#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino.h>
#include <Arduino_JSON.h>
const char* ssid = "Son Anh";
const char* password = "01102001";
const char* serverControlMode = "http://192.168.0.2:8080/api/v1/control-modes/lamp";
const char* serverLight = "http://192.168.0.2:8080/api/v1/lights";
const char* serverSound = "http://192.168.0.2:8080/api/v1/sounds";
const int RELAYPIN = 23;
String controlReadings;
String lightReadings;
String soundReadings;
void setup() {
  Serial.begin(9600);
  pinMode(RELAYPIN, OUTPUT);
  //setup wifi
  WiFi.begin(ssid,password);
  Serial.println("conecting");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());  
}

void loop() {
  delay(3000);
  if(WiFi.status()== WL_CONNECTED){            
      controlReadings = httpGETRequest(serverControlMode);
      JSONVar control = JSON.parse(controlReadings);
      Serial.println(JSON.stringify(control["operationMode"]));
      Serial.println(JSON.stringify(control["manualMode"]));
      if(JSON.stringify(control["operationMode"]).equals("\"MANUAL\"")) {
        Serial.println(JSON.stringify(control["manualMode"]));
        if(JSON.stringify(control["manualMode"]).equals("\"OFF\"")) {
          Serial.println("HIGH");
          digitalWrite(RELAYPIN,HIGH);
        } else {
          Serial.println("LOW");
          digitalWrite(RELAYPIN,LOW);
        }
      } else if (JSON.stringify(control["operationMode"]).equals("\"SOUND\"")) {
        soundReadings = httpGETRequest(serverSound);
        JSONVar sound = JSON.parse(soundReadings);
        Serial.println(JSON.stringify(sound["soundMode"]));
        if(JSON.stringify(sound["soundMode"]).equals("\"OFF\"")) {
          Serial.println("HIGH");
          digitalWrite(RELAYPIN,HIGH);
        } else {
          Serial.println("LOW");
          digitalWrite(RELAYPIN,LOW);
        }
      } else {
        lightReadings = httpGETRequest(serverLight);
        JSONVar light = JSON.parse(lightReadings);
        Serial.println(JSON.stringify(light["digitalRead"]));
        if(JSON.stringify(light["digitalRead"]).equals("0")) {
          digitalWrite(RELAYPIN,HIGH);
        } else {
          digitalWrite(RELAYPIN,LOW);
        }
      }
    }
    else {
      Serial.println("WiFi Disconnected");
    }
}
String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  // If you need Node-RED/server authentication, insert user and password below
  //http.setAuthorization("REPLACE_WITH_SERVER_USERNAME", "REPLACE_WITH_SERVER_PASSWORD");
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}
