#include <Arduino.h>
#include "DHT.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <string.h>
const char* ssid = "XXXXXXX";
const char* password = "XXXXXXXXX";
const char* serverName = "http://192.168.0.2:8080/api/v1/dhts";
#define DHTPIN 5     // Digital pin connected to the DHT sensor
 
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
 
DHT dht(DHTPIN, DHTTYPE);
void setup() {
  Serial.begin(9600);

 
  dht.begin();
    //setup wifi
  WiFi.begin(ssid,password);
  Serial.println("conecting");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}
 
void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.println(" %");
  Serial.print(F("Temperature: "));
  Serial.print(t);
  Serial.println(" oC");
  if(WiFi.status() == WL_CONNECTED) {
    WiFiClient client;
    HTTPClient http;
    http.begin(client, serverName);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST("{\"temperature\":\"" + String(t) 
    + "\",\"humidity\":\"" + String(h) + "\"}");
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
        
      // Free resources
      http.end();
  } else {
    Serial.println("WiFi Disconnected");
  }
}
 